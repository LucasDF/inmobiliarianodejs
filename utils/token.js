const jwt = require('jsonwebtoken');
const EXPIRATION_TIME = '2 hours';
const SECRET = 'INMOBILIARIA-secret.key-288';

module.exports = class Token {
    
    static generate(login) {
        let token = jwt.sign({login: login}, SECRET, {expiresIn: EXPIRATION_TIME});
        return token;
    }

    static validate(token) {
        try {
            token = token.replace('Bearer ', '');
            let result = jwt.verify(token, SECRET);
            return result;
        } catch (e) {}
    }

    static decode(token) {
        try {
            token = token.replace('Bearer ', '');
            return jwt.decode(token);
        } catch (e) {}
    }

    static getSecret() {
        return SECRET;
    }
}