
let deleteInmueble = () => {
    let id = document.getElementById('inmueble_id').innerText;
    $.ajax({
        url:"/inmuebles/" + id,
        type:"DELETE",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('token'));
        },
        success: function(data) {
            console.log(data);
            if (data.ok) {
                window.location.replace('/inmuebles');
            } else {
                window.location.replace('/');
            }
        }
    });
}

let login = () => {
    $.ajax({
        url:"/usuarios/login",
        type:"POST",
        data: JSON.stringify({login: $("#login").val(), password: $("#password").val()}),
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success: function(data) {
            if (data.ok) {
                $("#loginError").css('display', 'none');
                localStorage.setItem("token", data.token);
                window.location.assign('/');
            } else {
                $("#loginError").css('display', 'block');
                $("#loginError").html(data.error);
            }
        }
    });
}

let logout = () => {
    localStorage.removeItem('token');
    window.location.assign('/login');
}

let nuevoInmueble = () => {
    $.ajax({
        url:"/inmuebles/nuevo",
        type:"GET",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('token'));
        },
        success: function(data) {
            $("html").html(data);
        }
    });
}