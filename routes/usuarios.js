const express = require('express');
const md5 = require('md5');
const Tipo = require('../models/tipo');
const Usuario = require('../models/usuario');
const Token = require('../utils/token');

const router = express.Router();

// User registration
router.post('/registro', (req, res) => {
    let userJSON = req.body;

    if (userJSON.password !== userJSON.password2) {
        res.render('auth/registro', {user: userJSON, registroError: 'Las contraseñas deben coincidir'});
    }

    let user = new Usuario({
        nombre: userJSON.nombre,
        login: userJSON.login,
        password: md5(userJSON.password)
    });

    user.save().then(value => {
        res.redirect('../login');
    }).catch(err => {
        res.render('auth/registro', {user: userJSON, registroError: err});
    });
});

// Check user login
router.post('/login', (req, res) => {
    let credentials = req.body;
    if (!credentials.login || !credentials.password) {
        res.send({ ok: false, error: 'Debes especificar los campos login y password'});
    } else {
    Usuario.findOne({login: credentials.login, password: md5(credentials.password)})
        .then(user => {
            if (user) // Logged in successfully
                res.send({ ok: true, token: Token.generate(credentials.login)})    
            else
                res.send({ ok: false, error: 'Credenciales incorrectas'});
        }).catch(err => {
            res.send({ ok: false, error: 'Ha ocurrido un error en la base de datos'});
        });
    }
});

module.exports = router;