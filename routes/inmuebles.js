const express = require('express');
const passport = require('passport');
const fs = require('fs');
const Tipo = require('../models/tipo');
const Inmueble = require('../models/inmueble');

const router = express.Router();

// Returns view with all elements. Also can filter through query string
router.get('/', (req, res) => {
    let precio = Number.parseInt(req.query.precio);
    let superficie = Number.parseInt(req.query.superficie);
    let habitaciones = Number.parseInt(req.query.habitaciones);

    let query = Inmueble.find();

    if (precio && precio !== 0) {
        query.where('precio').lte(precio);
    }

    if (superficie && superficie !== 0) {
        query.where('superficie').gte(superficie);
    }

    if (habitaciones && habitaciones !== 0) {
        query.where('habitaciones').gte(habitaciones);
    }

    query.populate('tipo').then(inmuebles => {
        res.render('listar_inmuebles', {inmuebles});
    }).catch(err => {
        res.render('listar_inmuebles', {inmuebles: []});
    });
});

// Returns view with new building form
router.get('/nuevo', 
    passport.authenticate('jwt', {session: false, failureRedirect: '/error403'}),
    (req, res) => {
    Tipo.find()
        .then(tipos => res.render('nuevo_inmueble', {tipos}))
        .catch(err => res.render('500', {error: 'Error al obtener tipos de inmuebles'}));
});

const getExtension = (imageName = '') => {
    let parts = imageName.split('.');
    return parts[parts.length - 1];
}

// Creates new building
router.post('/', (req, res) => {
    let inmuebleJSON = req.body;
    if (!req.files.inmueble_imagen)
        res.send({ok: false, error: 'Campo imagen no puede estar vacío'})
    
    let extension = getExtension(req.files.inmueble_imagen.name);
    let fileName = req.files.inmueble_imagen.md5 + "." + extension;

    let inmueble = new Inmueble({
        descripcion: inmuebleJSON.inmueble_descripcion,
        tipo: inmuebleJSON.inmueble_tipo,
        habitaciones: inmuebleJSON.inmueble_habitaciones,
        superficie: inmuebleJSON.inmueble_superficie,
        precio: inmuebleJSON.inmueble_precio,
        imagen: fileName
    });

    // Saves in DB and creates image file in public folder
    inmueble.save().then(value => {
        let imagePath = __dirname + '/../public/img/uploads/' + fileName;

        req.files.inmueble_imagen.mv(imagePath,
        (err) => {
            if (err) {
                return res.status(500).render('500', {error: err});
            }
            res.redirect('../inmuebles');
        });
    }).catch(err => {
        res.send({ok: false, error: 'Error al validar campos'})
    });
});

// Returns view with building details
router.get('/:id', (req, res) => {
    let id = req.params.id;
    Inmueble.findOne({_id: id})
        .populate('tipo')
        .then(inmueble => {
            res.render('ficha_inmueble', {inmueble});
        }).catch(err => {
            res.redirect('../inmuebles');
        });
});

// Deletes specified building
router.delete('/:id', 
    passport.authenticate('jwt', {session: false, failureRedirect: '/error403'}),
    (req, res) => {
        let id = req.params.id;
        Inmueble.findByIdAndRemove(id)
            .populate('tipo')
            .then(inmueble => {
                let imagePath = __dirname + '/../public/img/uploads/' + inmueble.imagen;
                fs.unlink(imagePath, err => {
                    if (err) {
                        res.send({ok: false, error: 'Error al eliminar imagen'});
                    }
                    res.send({ok: true, inmueble: inmueble});
                });
            }).catch(err => {
                res.send({ok: false, error: 'Error al eliminar inmueble'})
            });
    }
);

// Returns view with buildings of specified type
router.get('/tipo/:idTipo', (req, res) => {
    let idTipo = req.params.idTipo;
    Inmueble.find({tipo: idTipo})
        .populate('tipo')
        .then(inmuebles => {
            res.render('listar_inmuebles', { inmuebles });
        }).catch(err => {
            res.redirect('../inmuebles');
        });
});

module.exports = router;
