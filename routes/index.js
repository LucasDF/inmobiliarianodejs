const express = require('express');
const Tipo = require('../models/tipo');

const router = express.Router();

// Render homepage
router.get('/', (req, res) => {
    Tipo.find()
        .then(tipos => {
            res.render('index', {tipos});
        }).catch(err => {
            res.render('index'), {tipos: []};
        });    
});

// Render login view
router.get('/login', (req, res) => {
    res.render('auth/login');
});

// Render register view
router.get('/registro', (req, res) => {
    res.render('auth/registro', {user: null, registroError: null});
});

// Render Error 403 view (Forbidden access)
router.get('/error403', (req, res) => {
    res.render('403');
});

router.delete('/error403', (req, res) => {
    res.render('403');
});

module.exports = router