const express = require('express');
const Tipo = require('../models/tipo');

const router = express.Router();

router.get('/', (req, res) => {
    Tipo.find()
        .then(result => res.send(result))
        .catch(err => res.send(err));
});

module.exports = router;