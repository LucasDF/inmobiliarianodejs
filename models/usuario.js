const mongoose = require('mongoose');

const UsuarioSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    }, 
    login: {
        type: String,
        unique: true,
        required: true,
        minlength: 1,
        trim: true
    },
    password: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    }
});

const Usuario = mongoose.model('Usuario', UsuarioSchema);

module.exports = Usuario;