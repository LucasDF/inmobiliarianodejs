const mongoose = require('mongoose');

const InmuebleSchema = new mongoose.Schema({
    descripcion: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    }, 
    tipo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tipo',
        required: true
    },
    habitaciones: {
        type: Number,
        required: true,
        min: 1
    },
    superficie: {
        type: Number,
        required: true,
        min: 25
    },
    precio: {
        type: Number,
        required: true,
        min: 10000
    },
    imagen: {
        type: String,
        required: false
    }
});

const Inmueble = mongoose.model('Inmueble', InmuebleSchema);

module.exports = Inmueble;