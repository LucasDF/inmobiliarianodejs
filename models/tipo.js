const mongoose = require('mongoose');

const TipoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    }
});

const Tipo = mongoose.model('Tipo', TipoSchema);

module.exports = Tipo;