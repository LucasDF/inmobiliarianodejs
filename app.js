const express = require('express');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const passport = require('passport');
const {Strategy, ExtractJwt} = require('passport-jwt');
const Token = require('./utils/token');
const index = require('./routes/index');
const inmuebles = require('./routes/inmuebles');
const tipos = require('./routes/tipos');
const usuarios = require('./routes/usuarios')

// Database connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/sv_inmobiliaria');

passport.use(new Strategy({
    secretOrKey: Token.getSecret(),
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}, (payload, done) => {
    if (payload.login) 
        return done(null, {login: payload.login});
    else
        return done(new Error('Usuario incorrecto'), null);
}));

let app = express();

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use((req, res, next) => {
    res.locals.error = "Error no especificado";
    next();
});
app.use('/public', express.static( __dirname + '/public'));
app.use(passport.initialize());
app.use(bodyParser.json());
app.use(fileUpload());

// Index routes
app.use('/', index);

// Buildings routes
app.use('/inmuebles', inmuebles);

// Building types routes
app.use('/tipos', tipos);

// Users routes
app.use('/usuarios', usuarios);

// Route does not match any of above: Error 404
app.use((req, res, next) => {
    res.status(404).render('404');
});

module.exports.app = app;